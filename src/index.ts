import './polyfill';
import { init } from './init';
import './styles.scss';

init();
